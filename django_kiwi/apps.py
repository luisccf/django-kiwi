from django.apps import AppConfig


class DjangoKiwiConfig(AppConfig):
    name = 'django_kiwi'
